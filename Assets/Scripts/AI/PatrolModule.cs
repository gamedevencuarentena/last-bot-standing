﻿using Assets.Scripts.Systems;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.AI
{
    public class PatrolModule : MonoBehaviour, AIModule
    {
        private MovementSystem movementSystem;

        public void Initialize(AIModuleProvider aiModuleProvider, SystemProvider systemProvider) => 
            this.movementSystem = systemProvider.Find<MovementSystem>();

        public void StartPatrol() =>
            movementSystem.MoveTo(RandomTarget, StartPatrol);

        public Vector3 RandomTarget => 
            new Vector3(Random.Range(-50, 50), 0, Random.Range(-50, 50));

        public void Disable() => 
            StopAllCoroutines();
    }
}
