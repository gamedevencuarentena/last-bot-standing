﻿using Assets.Scripts.Systems;
using UnityEngine;

namespace Assets.Scripts.Weapons
{
    public class CannonShot : MonoBehaviour
    {
        public GameObject ExplosionPrefab;
        public Rigidbody Rigidbody;

        public void Initialize(float shotForce) => 
            Rigidbody.AddForce(transform.forward * shotForce);

        private void OnTriggerEnter(Collider other)
        {
            other.GetComponent<HealthSystem>()?.ReceiveDamage();
            Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
            Rigidbody.isKinematic = true;
            Destroy(gameObject, .3f);
        }
    }
}
