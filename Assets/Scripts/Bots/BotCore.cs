﻿using System;
using Assets.Scripts.AI;
using Assets.Scripts.Directors;
using Assets.Scripts.Systems;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Bots
{
    public class BotCore : MonoBehaviour
    {
        public Brain Brain;

        private SystemProvider systemProvider;
        Action<BotCore> onDestroyed;
        
        public void Initialize(TargetProvider targetProvider)
        {
            systemProvider = new SystemProvider(this, GetComponentsInChildren<BotSystem>(), targetProvider);
            onDestroyed = targetProvider.OnBotDestroyed;
            Brain.Initialize(systemProvider);
            SetTankColor();
        }

        private void SetTankColor()
        {
            var color = Random.ColorHSV();
            GetComponent<TankColor>().SetColor(color);
        }

        public void Disable()
        {
            onDestroyed(this);
            Brain.Disable();
        }

        public void RefreshTarget() => 
            Brain.RefreshTarget();
    }
}