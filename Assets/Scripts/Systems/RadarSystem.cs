﻿using System.Collections.Generic;
using Assets.Scripts.Bots;
using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Systems
{
    public class RadarSystem : MonoBehaviour, BotSystem
    {
        public float Range;
        public float DisengageRange;

        
        private TargetProvider targetProvider;
        private BotCore bot;

        public bool TargetOutOfRange(Transform target) =>
            Vector3.Distance(target.position, bot.transform.position) > DisengageRange;

        public void Initialize(SystemProvider systemProvider)
        {
            targetProvider = systemProvider.TargetProvider;
            bot = systemProvider.Bot;
        }
        
        public void Disable() => enabled = false;

        public IEnumerable<BotCore> Scan() =>
            targetProvider.AnyBotInRange(bot, Range)
                ? targetProvider.AllBotsInRange(bot, Range)
                : new BotCore[0];

        public BotCore ScanForClosest() =>
            targetProvider.AnyBotInRange(bot, Range)
                ? targetProvider.ClosestEnemyInRange(bot, Range)
                : null;

        public bool IsStillAlive(Transform activeTarget) =>
            targetProvider.StillAlive(transform.GetComponent<BotCore>());
    }
}
